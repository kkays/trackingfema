""" Document.py : Contains the Document class, which represents a document as output from the data parsing segment."""
import nltk

class Document(object):
    """
    Represents a document, and handles loading documents from text files via the init method.
    """

    LINE_HEADER = 0
    LINE_SUBHEADER = 1
    LINE_AUTHOR = 2
    LINE_DATE = 3
    LINE_CONTENT = 4

    filename = ""
    """The name of the parsed file this was loaded from"""

    disaster_id = ""
    """The name of the disaster the document relates to"""

    disaster_nid = ""
    """The number of the document in the disaster listing"""

    header = ""
    """The document's header, if specified"""

    subheader = ""
    """The document's subheader, if specified"""

    author = ""
    """The document's author, if specified"""

    date = ""
    """The document's date, if specified"""

    content = ""
    """The document's raw content, if specified"""

    words = list()
    """A list of words created with nltk.word_tokenize"""

    def __init__(self, filename):
        self.filename = filename
        self.disaster_id, self.disaster_nid = filename.rsplit("#", 1)
        with open(filename, "r") as docfile:
            raw_data = docfile.readlines()
            self.header = raw_data[self.LINE_HEADER].strip()
            self.subheader = raw_data[self.LINE_SUBHEADER].strip()
            self.author = raw_data[self.LINE_AUTHOR].strip()
            self.date = raw_data[self.LINE_DATE].strip()
            self.content = raw_data[self.LINE_CONTENT].strip()
            self.words = nltk.word_tokenize(self.content)
            self.sentences = nltk.sent_tokenize(self.content)

    def __hash__(self):
        return hash(self.filename)

    def __eq__(self, other):
        return self.filename == other.filename