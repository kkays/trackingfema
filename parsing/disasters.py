#!/usr/bin/python3
# Script to download the disaster pages from the archive-it repository.
# Currently, only the first page of articles for a disaster is downloaded.

import bs4
import os
import requests
import time

TIME_SLEEP = 5

s = requests.Session()
head = s.get("http://archive-it.org/organizations/156")
head.raise_for_status()
head_soup = bs4.BeautifulSoup(head.text)
for result in head_soup.find_all("div", class_="result-item"):
	time.sleep(TIME_SLEEP)
	anchor = result.find("h3").find("a")
	print(anchor.text.strip() + " " + anchor["href"])
	disaster_head = s.get("http://archive-it.org" + anchor["href"])
	disaster_head.raise_for_status()
	disaster_file = open("disasters/" + anchor.text.strip(), "w")
	disaster_file.write(disaster_head.text)
	disaster_file.close()
	os.mkdir("articles/" + anchor.text.strip())
