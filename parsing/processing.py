#!/usr/bin/python3
# Script to extract data from downloaded articles. Since this part is time-
# consuming and was often iterated during the project, the actual downloading
# of the articles was separated into other scripts.
#
# The functions ending with _finder (except increment_finder) all take in the
# BeautifulSoup representation of an article, as well as possibility other
# variables. They then use that to attempt to extract a piece of metadata from
# the article. If the metadata is found it's returned as a string, otherwise
# an empty string is returned. Finder functions with arguments other than the
# soup argument are considered generic, and are transformed into single-argument
# functions using the _gen functions.
#
# The functions ending with _gen are used to "fill in" the generic finder
# functions. They take in a set of arguments and return a lambda function. The
# lambda function takes in a soup argument and calls the generic finder function
# with the soup argument and the arguments from the _gen function.

import bs4
import pathlib
import random

finder_counts = {}

# Increment the stats of a particular finder function in the finder_counts
# directory. Takes in a string representing a particular finder.
def increment_finder(finder):
	if finder not in finder_counts:
		finder_counts[finder] = 0
	finder_counts[finder] += 1

def no_entry_title(attr):
	return attr != "entry-title"

def meta_finder(soup, key, value):
	selection = soup.select('meta[' + key + '="' + value + '"]')
	if selection:
		if "content" in selection[0].attrs:
			increment_finder("meta_" + key + "_" + value)
			return selection[0]["content"].replace("\n", "")
		if "value" in selection[0].attrs:
			increment_finder("meta_" + key + "_" + value)
			return selection[0]["value"].replace("\n", "")
		print(selection)
		raise
	return ""

def meta_finder_gen(key, value):
	return lambda soup: meta_finder(soup, key, value)

def tag_bare_finder(soup, tag):
	selection = soup.find(tag)
	if selection:
		increment_finder("tag_bare_" + tag)
		return selection.text.replace("\n", "")
	return ""

def tag_bare_finder_gen(tag):
	return lambda soup: tag_bare_finder(soup, tag)

def tag_finder(soup, tag, key, value):
	selection = soup.find(tag, attrs={key: value})
	if selection:
		increment_finder("tag_" + tag + "_" + key + "_" + str(value))
		return selection.text.replace("\n", "")
	return ""

def tag_finder_gen(tag, key, value):
	return lambda soup: tag_finder(soup, tag, key, value)

def span_ctl00_finder(soup):
	selection = soup.find("span", id="ctl00_PageContent_lblStoryTitle")
	if selection and selection.find("b"):
		increment_finder("span_ctl00")
		return selection.text.replace("\n", "")
	return ""

def h2_subhead_finder(soup):
	selection = soup.find("h2", class_="subhead")
	if selection:
		increment_finder("h2_subhead")
		return selection.find("p").text.replace("\n", "")
	return ""

def span_author_finder(soup):
	selection = soup.find("span", class_="author")
	if selection and selection.find("a"):
		increment_finder("span_author")
		return selection.find("a").text.replace("\n", "")
	return ""

# The _functions set of variables are four arrays used to find the metadata
# inside articles. Each item in the arrays is either a pointer fo a finder
# function or a call to a gen function.
title_functions = [
		meta_finder_gen("name", "Headline"),
		meta_finder_gen("name", "title"),
		meta_finder_gen("property", "og:title"),
		tag_finder_gen("h1", "class", no_entry_title),
		tag_finder_gen("span", "class", "headline"),
		tag_finder_gen("h2", "id", "ThreadTitle"),
		span_ctl00_finder,
		tag_finder_gen("div", "id", "article_title")]

subtitle_functions = [
		meta_finder_gen("name", "Description"),
		meta_finder_gen("name", "description"),
		meta_finder_gen("property", "og_description"),
		tag_bare_finder_gen("h2"), h2_subhead_finder]

author_functions = [
		meta_finder_gen("name", "author"),
		meta_finder_gen("name", "byl"),
		tag_finder_gen("div", "class", "story_byline"),
		span_author_finder,
		tag_finder_gen("span", "class", "name"),
		tag_finder_gen("div", "class", "byline"),
		tag_finder_gen("span", "class", "credit"),
		tag_finder_gen("cite", "class", "byline"),
		tag_finder_gen("a", "class", "author"),
		tag_finder_gen("p", "class", "byline"),
		tag_finder_gen("a", "title", "Posts by admin"),
		tag_finder_gen("p", "class", "organization"),
		tag_finder_gen("a", "title", "See all post by DipNote Bloggers"),
		tag_finder_gen("div", "class", "reporter"),
		tag_bare_finder_gen("strong"),
		tag_finder_gen("font", "class", "byline"),
		tag_finder_gen("a", "class", "ui-author"),
		tag_finder_gen("div", "class", "byline-author"),
		tag_finder_gen("span", "class", "fn")]

date_functions = [
		meta_finder_gen("name", "OriginalPublicationDate"),
		meta_finder_gen("name", "Date"),
		meta_finder_gen("property", "og:created_time"),
		meta_finder_gen("name", "eGMS.date.issued"),
		meta_finder_gen("name", "article_date_original"),
		meta_finder_gen("name", "DC.date.created"),
		meta_finder_gen("name", "created"),
		meta_finder_gen("name", "date"),
		tag_finder_gen("div", "class", "story_date"),
		tag_finder_gen("span", "class", "entry-date"),
		tag_finder_gen("span", "class", "postdate"),
		tag_finder_gen("span", "class", "pubdate"),
		tag_finder_gen("span", "class", "date"),
		tag_finder_gen("span", "class", "time"),
		tag_finder_gen("span", "class", "publishdate"),
		tag_finder_gen("b", "class", "cat_title"),
		tag_finder_gen("p", "class", "meta"),
		tag_finder_gen("p", "class", "story_by"),
		tag_finder_gen("span", "class", "ap_tm_stmp"),
		tag_finder_gen("span", "class", "title"),
		tag_finder_gen("span", "class", "ui-staffline"),
		tag_finder_gen("div", "class", "byline_publication_date"),
		tag_finder_gen("span", "class", "dateStamp")]

data_found = []
found_count = [0, 0, 0, 0]

for folder_path in pathlib.Path("articles").glob("*"):
	index = 0
	for file_path in folder_path.glob("*"):
		soup = bs4.BeautifulSoup(file_path.open().read())
		out_name = "processed/" + folder_path.name + " #" + str(index)
		processed_file = open(out_name, "w")
		single_data_found = [folder_path.name, file_path.name, False, False, False, False, out_name]

		# Actually pull out the metadata. The _functions arrays are
		# iterated through, and the functions within are called with
		# the BeautifulSoup representation of the article. Each loop
		# ends once a function finds the metadata it was looking for.
		title_found = False
		for function in title_functions:
			selection = function(soup)
			if selection != "":
				single_data_found[2] = True
				found_count[0] += 1
				processed_file.write(selection)
				processed_file.write("\n")
				title_found = True
				break
		for function in subtitle_functions:
			selection = function(soup)
			if selection != "":
				single_data_found[3] = True
				found_count[1] += 1
				processed_file.write(selection)
				break
		# If we didn't find a title, any subtitle found is promoted to
		# title.
		if not title_found:
			processed_file.write("\n")
		processed_file.write("\n")
		for function in author_functions:
			selection = function(soup)
			if selection != "":
				single_data_found[4] = True
				found_count[2] += 1
				processed_file.write(selection)
				break
		processed_file.write("\n")
		for function in date_functions:
			selection = function(soup)
			if selection != "":
				single_data_found[5] = True
				found_count[3] += 1
				processed_file.write(selection)
				break
		processed_file.write("\n")

		# Extract the actual text of the article.
		if soup.body is None:
			continue
		texts = soup.body.find_all(text=True)
		for text in texts:
			text_string = str(text).replace("\n", " ").replace("\t", " ").strip()
			if text.parent.name not in ['style', 'script', '[document]', 'head', 'title', 'wb-div', 'wb_div', 'a'] and not isinstance(text, bs4.element.Comment):
				processed_file.write(text_string + " ")
		processed_file.write("\n")

		# Extract the anchor texts of any links found in the article.
		anchor_texts = soup.body.find_all("a", text=True)
		for text in anchor_texts:
			text_string = str(text.text).replace("\n", " ").replace("\t", " ").strip()
			processed_file.write(text_string + " ")

		data_found.append(single_data_found)
		found_count[0] += 1
		index += 1

#for single_data_found in data_found:
#	if not single_data_found[4] and not single_data_found[5]:
#		print(single_data_found)
#print(found_count)
#print(selection_counts)
#for key, value in selections.items():
#	print(key)
#	print(value)
#print("AUDIT:")
#random.shuffle(data_found)
#for x in range(20):
#	print(data_found[x])
#for finder in finder_counts.keys():
#	print("%s: %d" % (finder, finder_counts[finder]))
