#!/usr/bin/python3
# Script to download the archived articles for all of the disasters in the
# archive-it repository. Prints out statistics about what was downloaded after
# completion.

import bs4
import os.path
import pathlib
import requests
import time

TIME_SLEEP = 5

already_downloaded = 0
success = 0
status404 = 0
failure = 0

s = requests.Session()
for file_path in pathlib.Path("disasters/").glob("*"):
	print(file_path)
	soup = bs4.BeautifulSoup(file_path.open().read())
	for disaster_result in soup.find_all("div", class_="result-item"):
		article_title = disaster_result.find("h3").text[18:-8].replace("/", "")
		if disaster_result.find("h3").find("a") is not None:
			article_title = disaster_result.find("h3").find("a")["href"].replace("/", "")

		article_url = ""
		if len(disaster_result.find_all("a")) == 2 and len(disaster_result.find_all("h3", class_="url")) == 2:
			article_url = disaster_result.find_all("a")[1]["href"]
		elif len(disaster_result.find("p", class_="waybackCaptureInfo").find_all("a")) == 1:
			article_url = disaster_result.find("p", class_="waybackCaptureInfo").find_all("a")[0]["href"]
		elif len(disaster_result.find("p", class_="waybackCaptureInfo").find_all("a")) > 2:
			article_url = disaster_result.find("p", class_="waybackCaptureInfo").find_all("a")[1]["href"]

		if article_title != "" and article_url != "":
			article_filename = file_path.name + "/" + article_title
			if os.path.isfile("articles/" + article_filename):
				already_downloaded += 1
				continue
			print(article_filename + " " + article_url)
			try:
				article = s.get(article_url)
			except:
				failure += 1
				continue
			time.sleep(TIME_SLEEP)
			if article.status_code == 404:
				status404 += 1
				continue
			try:
				article.raise_for_status()
			except:
				failure += 1
				continue
			try:
				article_file = open("articles/" + article_filename, "w")
			except OSError:
				failure += 1
				continue
			article_file.write(article.text)
			article_file.close()
			success += 1

print("Articles already downloaded: %d" % already_downloaded)
print("Articles downloaded: %d" % success)
print("404 errors: %d" % status404)
print("Other failures: %d" % failure)
