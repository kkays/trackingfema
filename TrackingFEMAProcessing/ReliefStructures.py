"""ReliefStructures.py : Contains data structures for parties involved in relief efforts"""

from enum import Enum
import logging

logging.basicConfig(level=logging.INFO)  # Comment this line out to silence the script


class NamedEntity(object):
    """
    Interface for entities with proper names
    """

    NAMES_FILE = ""
    """Name of the file that contains the names for this entity"""

    @staticmethod
    def load_names(ne_class):
        """Loads names for the given named entity class"""
        with open(ne_class.NAMES_FILE, "r") as names_file:
            for line in names_file:
                ne_class.names.add(line.strip())

    name = ""
    """The actual matching name from NamedEntity.names"""

    def __init__(self, name):
        self.name = name

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return self.name


class Location(NamedEntity):
    """
    Represents a location
    """
    NAMES_FILE = "Locations.txt"
    names = set()

    def __init__(self, name):
        NamedEntity.__init__(self, name)


class Person(NamedEntity):
    """
    Represents a person
    """
    NAMES_FILE = None
    names = set()

    def __init__(self, name):
        NamedEntity.__init__(self, name)


class Agency(NamedEntity):
    """
    Represents an agency
    """
    NAMES_FILE = "Agencies.txt"
    names = set()

    def __init__(self, name):
        NamedEntity.__init__(self, name)


class ReliefType(Enum):
    """
    (See http://www.irs.gov/Charities-&-Non-Profits/Charitable-Organizations/Disaster-Relief:-Types-of-Assistance-Charity-May-Provide
    for a good list of types)
    """
    UNCLASSIFIED = 0
    Food = 1
    Clothing = 2
    Housing = 3
    Transportation = 4
    Medical = 5
    CashEquivalent = 6


class Relief():
    """
    Represents aid provided
    """
    long_term = False
    """Whether aid is long-term"""

    amount = 0
    """Amount of relief given, in units 'unit'"""

    unit = ""
    """Unit for amount of relief (dollars, # cans of beans, etc)"""

    type = ReliefType.UNCLASSIFIED
    """Type of relief provided"""

logging.info("Loading location names.")
NamedEntity.load_names(Location)
logging.info("Loading agency names.")
NamedEntity.load_names(Agency)
logging.info("Locations loaded: \n" + str(Location.names) + "\n")
logging.info("Agencies loaded: \n" + str(Agency.names) + "\n")
