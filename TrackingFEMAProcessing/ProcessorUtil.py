import os
import logging
import itertools
import collections
import copy


def entity_mentions_by_disaster(references):
    """Returns a dict of disasters from a dict of references: {disaster: {entity_name: count}}"""
    disaster_dict = dict()
    for doc_name, ref_dict in references.items():
        disaster_name = doc_name.rsplit("#", 1)[0]
        for entity_name, count in ref_dict.items():
            if disaster_name in disaster_dict:
                try:
                    disaster_dict[disaster_name][entity_name] += int(count)
                except KeyError:
                    disaster_dict[disaster_name][entity_name] = int(count)
            else:
                disaster_dict[disaster_name] = {entity_name: int(count)}
    return disaster_dict


def write_entity_mentions_by_disaster(references, entity_type, summary_dir):
    """Makes it easy to represent the number of location mentions
    Creates a file with the following format:
    First line: document_name number_of_locations
    Second line: location_name occurrence_count"""
    # First, we need to get a dict
    disaster_dict = entity_mentions_by_disaster(references)

    # Then, we need to print out that dict
    with open(os.path.join(summary_dir, entity_type + "_mentions_by_disaster"), "w") as summary_file:
        for disaster_name, ref_dict in disaster_dict.items():
            summary_file.write(disaster_name + " " + str(len(ref_dict)) + "\n")
            for entity_name, count in ref_dict.items():
                summary_file.write(entity_name + " " + str(count) + "\n")



def entity_relationships_by_disaster(references):
    """Represents the relationships between entities for each disaster
    Two entities have a relationship when they are mentioned in the same article
    Duplicate entity pairs are listed as a way of gauging relationship strength
    returns <disaster: [[entity1, entity2], [entity1, entity2]]>
    """
    assocs = dict(list(set())) #
    for doc_name, ref_dict in references.items():
        disaster_name = doc_name.rsplit("#", 1)[0]
        if disaster_name not in assocs:
            assocs[disaster_name] = list()
        assocs[disaster_name].extend(itertools.combinations(ref_dict.keys(), 2))
    return assocs


def write_entity_relationships_by_disaster(references, entity_type, summary_dir):
    assocs = entity_relationships_by_disaster(references)
    with open(os.path.join(summary_dir, entity_type + "_relationships_by_disaster"), "w") as summary_file:
        for disaster_name, relationships in assocs.items():
            # First, combine associations
            sorted_relations = list()
            for relationship in relationships:
                sorted_relations.append(",".join(sorted(relationship)))
            # Then, find the number of time they're repeated
            counted = collections.Counter(sorted_relations)
            # Now, print it out
            summary_file.write(disaster_name + " " + str(len(counted.most_common())) + "\n")
            for relationship, count in counted.most_common():
                summary_file.write(relationship + " " + str(count) + "\n")


def combine_references(references_a, references_b):
    references_c = copy.deepcopy(references_a)
    for filename, ref_dict in references_b.items():
        try:
            references_c[filename].update(copy.deepcopy(ref_dict))
        except KeyError:
            references_c[filename] = copy.deepcopy(ref_dict)
    return references_c