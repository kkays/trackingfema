"""Processor.py : Finds relationships between relief objects in article documents."""
import os
import argparse
import multiprocessing
import threading
import logging
import nltk
from Document import Document
from ReliefStructures import Location, Person, Agency, NamedEntity, Relief, ReliefType
from ProcessorUtil import *

logging.basicConfig(level=logging.INFO)  # Comment this line out to silence the script
OUT_DIR = os.path.join(os.path.dirname(__file__), "Output")
SUMMARY_DIR = os.path.join(os.path.dirname(__file__), "Summary")


def get_entities(named_entity_class, document):
    ref_dict = dict()
    for name in named_entity_class.names:
        if name in document.content:
            entity = named_entity_class(name)
            try:
                ref_dict[name] += 1
            except KeyError:
                ref_dict[name] = 1
    return ref_dict


# adapted from http://timmcnamara.co.nz/post/2650550090/extracting-names-with-6-lines-of-python-code
def get_nltk_nes(document, node_type):
    """A generator that yields the names of objects of type node_type"""
    for sentence in document.sentences:
        for chunk in nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(sentence))):
            if hasattr(chunk, "label") and chunk.label() == node_type:
                yield [leaf[0] for leaf in chunk.leaves()]


def get_people(names, document):
    """Gets people based on a list of their names"""
    people = dict()
    for name in names:
        person = Person(" ".join(name))
        try:
            people[person] += 1
        except KeyError:
            people[person] = 1
    return people


def generate_meta(document):
    locations = get_entities(Location, document)
    agencies = get_entities(Agency, document)
    people = get_people(get_nltk_nes(document, "PERSON"), document)
    # Write each dictionary's contents to a length-delimited file
    with open(os.path.join(OUT_DIR, os.path.basename(document.filename)), "w") as outfile:
        # Write the locations
        outfile.write(str(len(locations.keys())) + "\n")
        for location, count in locations.items():
            outfile.write(str(location) + " " + str(count) + "\n")

        # Write the agencies
        outfile.write(str(len(agencies.keys())) + "\n")
        for agency, count in agencies.items():
            outfile.write(str(agency) + " " + str(count) + "\n")

        # Write the people
        outfile.write(str(len(people.keys())) + "\n")
        for person, count in people.items():
            outfile.write(str(person) + " " + str(count) + "\n")


def process_file(filename):
    logging.info("Loading article %s", filename)
    try:
        doc = Document(os.path.join("Articles", filename))
    except UnicodeDecodeError as ude:
        logging.warning("Unable to load file %s: \"%s\"", filename, ude.reason)
        return
    generate_meta(doc)


def get_from_meta(filename):
    locations = dict()
    agencies = dict()
    people = dict()
    with open(os.path.join(OUT_DIR, filename), 'r') as meta_file:
        # First line tells us how many locations
        first_line = meta_file.readline()
        num_locations = 0
        try:
            num_locations = int(first_line)
        except ValueError as e:
            logging.warning("Unable to read file %s due to: unable to parse \"%s\"", filename, first_line.strip())
            return None, None, None
        for i in range(num_locations):
            location = meta_file.readline().strip().rsplit(" ", 1)
            locations[location[0]] = location[1]

        # Second line tells us how many agencies
        for i in range(int(meta_file.readline())):
            agency = meta_file.readline().strip().rsplit(" ", 1)
            agencies[agency[0]] = agency[1]

        # Third line tells us how many people
        for i in range(int(meta_file.readline())):
            person = meta_file.readline().strip().rsplit(" ", 1)
            people[person[0]] = person[1]

    return tuple([{filename: locations}, {filename: agencies}, {filename: people}])


def multiprocess_meta():
    # Make a pool using a process for all but one of the cores (leave one for us)
    p = multiprocessing.Pool(multiprocessing.cpu_count() - 1)
    # Go through each file in an "Articles" sub directory
    dirs = os.listdir("Articles")
    # Spread the workload across each processor
    num_chunks = int(len(dirs) / (multiprocessing.cpu_count() - 1))
    logging.info("Multiprocessing %d files in groups of %d distributed across %d processes.",
                 len(dirs), num_chunks, multiprocessing.cpu_count())
    p.map(process_file, dirs, num_chunks)


def multithread_util(task_name, function, *args):
    logging.info("Processing " + task_name)
    function(*args)
    logging.info("Finished processing " + task_name)


def process(read_articles=False, write_summaries=False):
    # Create the output directory if needed
    if not os.path.exists(OUT_DIR):
        os.mkdir(OUT_DIR)

    # First, process each article to a smaller, quick-to-read file
    if read_articles:
        multiprocess_meta()

    # Second, read those processed files
    # The dicts have the format: {filename: {entity_name: occurrence_count}}
    locations = dict()
    agencies = dict()
    people = dict()
    for locations_part, agencies_part, people_part in map(get_from_meta, os.listdir(OUT_DIR)):
        if locations_part is None or agencies_part is None or people_part is None:
            continue
        locations.update(locations_part)
        agencies.update(agencies_part)
        people.update(people_part)

    # Third, write summary information
    if write_summaries:
        if not os.path.exists(SUMMARY_DIR):
            os.mkdir(SUMMARY_DIR)

        loc_age = combine_references(locations, agencies)
        loc_peo = combine_references(locations, people)
        age_peo = combine_references(people, agencies)
        all_references = combine_references(loc_age, people)

        threads = list()

        threads.append(threading.Thread(target=multithread_util, args=(
            "location mentions", write_entity_mentions_by_disaster,
            locations, "location", SUMMARY_DIR)))

        threads.append(threading.Thread(target=multithread_util, args=(
            "agency mentions", write_entity_mentions_by_disaster,
            agencies, "agency", SUMMARY_DIR)))

        threads.append(threading.Thread(target=multithread_util, args=(
            "person mentions", write_entity_mentions_by_disaster,
            people, "person", SUMMARY_DIR)))

        threads.append(threading.Thread(target=multithread_util, args=(
            "location relationships", write_entity_relationships_by_disaster,
            locations, "location", SUMMARY_DIR)))

        threads.append(threading.Thread(target=multithread_util, args=(
            "agency relationships", write_entity_relationships_by_disaster,
            agencies, "agency", SUMMARY_DIR)))

        threads.append(threading.Thread(target=multithread_util, args=(
            "person relationships", write_entity_relationships_by_disaster,
            people, "person", SUMMARY_DIR)))

        threads.append(threading.Thread(target=multithread_util, args=(
            "location, agency relationships", write_entity_relationships_by_disaster,
            loc_age, "location_agency", SUMMARY_DIR)))

        threads.append(threading.Thread(target=multithread_util, args=(
            "location, person relationships", write_entity_relationships_by_disaster,
            loc_peo, "location_person", SUMMARY_DIR)))

        threads.append(threading.Thread(target=multithread_util, args=(
            "person, agency relationships", write_entity_relationships_by_disaster,
            age_peo, "person_agency", SUMMARY_DIR)))

        threads.append(threading.Thread(target=multithread_util, args=(
            "location, person, agency relationships", write_entity_relationships_by_disaster,
            all_references, "location_agency_person", SUMMARY_DIR)))

        for t in threads:
            t.start()

        for t in threads:
            t.join()

    logging.info("Done")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--parse_articles", help="parse the raw article files", action="store_true")
    parser.add_argument("--write_summaries", help="Write the summary files for use in graphs", action="store_true")
    args = parser.parse_args()
    if args.parse_articles or args.write_summaries:
        process(read_articles=args.parse_articles, write_summaries=args.write_summaries)
    else:
        parser.print_help()
